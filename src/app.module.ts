import {Module} from '@nestjs/common';
import {AppController} from './app.controller';
import {AppService} from './app.service';
import {TypeOrmModule} from '@nestjs/typeorm';
import {ServeStaticModule} from '@nestjs/serve-static';
import {ConfigModule} from '@nestjs/config';
import {path} from 'app-root-path';
import { NewsModule } from './news/news.module';

@Module({
    imports: [
        ConfigModule.forRoot(),
        ServeStaticModule.forRoot({
            rootPath: `${path}/static/`,
        }),
        TypeOrmModule.forRoot({
            type: 'postgres',
            host: process.env.POSTGRES_HOST,
            port: +process.env.POSTGRES_PORT,
            username: process.env.POSTGRES_USER,
            password: '',
            database: process.env.POSTGRES_DATABASE,
            entities: ['dist/**/*.entity{.ts,.js}'],
            synchronize: true,
        }),
        NewsModule,
    ],
    controllers: [AppController],
    providers: [AppService],
})
export class AppModule {
}
