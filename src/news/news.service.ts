import {HttpException, HttpStatus, Injectable} from '@nestjs/common';
import {CreateNewsDto} from './dto/create-news.dto';
import {UpdateNewsDto} from './dto/update-news.dto';
import {InjectRepository} from "@nestjs/typeorm";
import {News} from "./entities/news.entity";
import {Repository} from "typeorm";

@Injectable()
export class NewsService {

    constructor(
        @InjectRepository(News)
        private readonly repository: Repository<News>
    ) {
    }

    create(createNewsDto: CreateNewsDto) {
        return this.repository.save(createNewsDto);
    }

    findAll() {
        return this.repository.find({
            order: {
                id: "DESC"
            }
        });
    }

    findOne(id: number) {
        try {
            return this.repository.findOneOrFail(id);
        } catch (e) {
            throw new HttpException(
                'Такой записи не существеут',
                HttpStatus.NOT_FOUND,
            );
        }
    }

    update(id: number, updateNewsDto: UpdateNewsDto) {
        try {
            const news = this.repository.findOneOrFail(id);

            return this.repository.save({
                id: id,
                ...updateNewsDto,
                ...news
            })

        } catch (e) {
            throw new HttpException(
                'Такой записи не существеут',
                HttpStatus.NOT_FOUND,
            );
        }
    }

    remove(id: number) {
        return this.repository.delete(id);
    }
}
