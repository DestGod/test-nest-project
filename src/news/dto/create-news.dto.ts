import {IsString, Length} from "class-validator";


export class CreateNewsDto {

    @IsString()
    @Length(5, 255)
    title: string;

}
